#include "myglwidget.h"
#include <QtOpenGL>
#include <QScopedPointer>
#if QT_VERSION >= 0x050000
#include <QWidget>
#else
#include <QtGui/QWidget>
#endif
#include <iostream>
#include <GL/glu.h>

MyGLWidget::MyGLWidget(QWidget *parent)
    : QGLWidget(QGLFormat(QGL::SampleBuffers), parent)
{
    xRot = 0;
    yRot = 0;
    zRot = 0;
    xTrans = 0;
    yTrans = 0;
    zTrans = 0;
    scale = 1;
    backgroundColor = Qt::white;
    nodeColor = Qt::blue;
    //lignColor = Qt::red;
    lignColor = Qt::green;
    //lignColor = Qt::black;
    listNodes << new Node( QVector3D(-1.0f, 1.0f, 0.0f), 2, 2 , nodeColor)
              //<< new Node( QVector3D(-1.2f, 1.2f, 0.0f), 2.4f, 2.4f , Qt::black)
              << new Node( QVector3D(-4.0f, 1.0f, 0.0f), 2, 2, nodeColor);
             // << new Node( QVector3D(-4.2f, 1.2f, 0.0f), 2.4f, 2.4f , Qt::black);
}

static void qNormalizeAngle(int &angle)
{
    while (angle < 0) {
        angle += 360;
    }
    while (angle > 360) {
        angle -= 360;
    }
}

void MyGLWidget::initializeGL()
{
    qglClearColor(Qt::white);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);
}

void MyGLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glScalef(scale, scale, scale);
    glTranslatef(xTrans, yTrans,zTrans);
    glRotatef(xRot, 1.0, 0.0, 0.0);
    glRotatef(yRot, 0.0, 1.0, 0.0);
    glRotatef(zRot, 0.0, 0.0, 1.0);
    draw();
}

void MyGLWidget::resizeGL(int width, int height)
{
    double range = 10;
    double aspect = width / height;
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
#ifdef QT_OPENGL_ES_1
    glOrthof(-0.5, +0.5, -0.5, +0.5, 4.0, 15.0);
#else
    glOrtho(-range,range,-range/aspect,range/aspect,-(range*500),range*5);
#endif
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

QSize MyGLWidget::minimumSizeHint() const
{
    return QSize(50, 50);
}

QSize MyGLWidget::sizeHint() const
{
    return QSize(400,400);
}

void MyGLWidget::mousePressEvent(QMouseEvent *event)
{
    lastPos = event->pos();
    //listNodes << new Node( QVector3D(event->x(),event->y(),0.0f),2,2,Qt::blue);
    //std::cout << event->x() << " " << event->y();
    // std::cout.flush();
}

void MyGLWidget::mouseMoveEvent(QMouseEvent *event)
{
    int dx = event->x() - lastPos.x();
    int dy = event->y() - lastPos.y();

    if (event->buttons() & Qt::LeftButton)
    {
        setXRotation(xRot - 2 * dy);
        setYRotation(yRot - 2 * dx);
    }

    lastPos = event->pos();
}

void MyGLWidget::contextMenuEvent(QContextMenuEvent *event)
{
    QScopedPointer<QMenu> contextMenu  (new QMenu( this));
    QList<QAction *> listActions;
    QAction *delActionNode = new QAction(tr("Delete Node"),this);
    QAction *addActionLine = new QAction(tr("New line"),this);
    QAction *delActionLine = new QAction(tr("Delete line"),this);
    QAction *addActionNode = new QAction(tr("New Node"),this);
    QAction *propertiesAction = new QAction(tr("Properties"),this);
    QAction *separator = new QAction(this);
    separator->setSeparator(true);
    QAction *separator2 = new QAction(this);
    separator2->setSeparator(true);

    listActions << addActionNode << delActionNode << separator << addActionLine << delActionLine << separator2 <<propertiesAction;
    contextMenu->addActions(listActions);
    contextMenu->exec(event->globalPos());
}

void MyGLWidget::wheelEvent(QWheelEvent *event)
{
    if (event->delta() > 0) {
        scale += scale*0.1f;
    } else {
        scale -= scale*0.1f;
    }
    updateGL();
}

void MyGLWidget::setXRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != xRot)
    {
        xRot = angle;
        emit xRotationChanged(angle);
        updateGL();
    }
}

void MyGLWidget::setYRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != yRot)
    {
        yRot = angle;
        emit yRotationChanged(angle);
        updateGL();
    }
}

void MyGLWidget::setZRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != zRot)
    {
        zRot = angle;
        emit zRotationChanged(angle);
        updateGL();
    }
}

void MyGLWidget::setXTranslation(int trans)
{
    if (trans != xTrans)
    {
        xTrans = trans;
        emit xTranslationChanged(trans);
        updateGL();
    }
}

void MyGLWidget::setYTranslation(int trans)
{
    if (trans != yTrans)
    {
        yTrans = trans;
        emit yTranslationChanged(trans);
        updateGL();
    }
}

void MyGLWidget::setZTranslation(int trans)
{
    if (trans != zTrans)
    {
        zTrans = trans;
        emit zTranslationChanged(trans);
        updateGL();
    }
}

void MyGLWidget::setBackgroundColor(QColor color)
{
    this->backgroundColor = color;
    emit backgroundColorChanged(color);
    updateGL();
}

void MyGLWidget::setLignColor(QColor color)
{
    this->lignColor = color;
    emit lignColorChanged(color);
    updateGL();
}

void MyGLWidget::setNodeColor(QColor color)
{
    this->nodeColor = color;
    foreach (Node* node, listNodes) {
        node->color = color;
    }
    //this->listNodes[activeSelectedNode]->color = color; TODO: an implementation we all want
    emit nodeColorChanged(color);
    updateGL();
}

void MyGLWidget::setScale(int scale)
{
    this->scale = scale * 0.1f;
    emit scaleChanged(scale * 0.1f);
    updateGL();
}

void MyGLWidget::draw()
{
    qglClearColor(this->backgroundColor);

    foreach (Node* node, listNodes) {
        qglColor(node->color);
        glBegin(GL_QUADS);                      // Draw A Quad
        glVertex3f(node->topLeft.x(),  node->topLeft.y(), node->topLeft.z());              // Top Left
        glVertex3f(node->topRight.x(),  node->topRight.y(), node->topRight.z());              // Top Right
        glVertex3f(node->bottomRight.x(),  node->bottomRight.y(), node->bottomRight.z());              // Bottom Right
        glVertex3f(node->bottomLeft.x(),  node->bottomLeft.y(), node->bottomLeft.z());              // Bottom Left
        glEnd();
    }

    qglColor(this->lignColor);
    glBegin(GL_LINES);
    glVertex3f(-2.0f, 0.0f, 0.0f);              // origin of the line
    glVertex3f(-1.0f, 0.0f, 0.0f);              // ending point of the line
    glEnd();
}
