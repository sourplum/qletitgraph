#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QScopedPointer>
#include "about.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QActionGroup *menuActionGroup = new QActionGroup(this);
    menuActionGroup->setExclusive(true);
    menuActionGroup->addAction(ui->action2D);
    menuActionGroup->addAction(ui->action3D);

    QActionGroup *menuActionGroupDev = new QActionGroup(this);
    menuActionGroupDev->setExclusive(true);
    menuActionGroupDev->addAction(ui->actionShow_Hide_Node);
    menuActionGroupDev->addAction(ui->actionSelect_Graph);

    on_actionSelect_Graph_toggled(true);
    on_actionShow_Hide_Node_toggled(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionOpen_triggered()
{
    QString filename = QFileDialog::getOpenFileName(this,
                                                    tr("Open Graph"), "/home", tr("Graph Files (*.dot *.graphml)"));
}

void MainWindow::on_actionAbout_QLetItGraph_triggered()
{
    About* about = new About(this);
    about->show();
}

void MainWindow::on_actionShow_Hide_Node_toggled(bool arg1)
{
    if (arg1) {
         ui->toolBox_apparence_node->show();
         ui->label_type->setText( tr("Node") );
         ui->lineEdit_id->setText( tr("node1") );
    } else {
         ui->toolBox_apparence_node->hide();
    }
}

void MainWindow::on_actionSelect_Graph_toggled(bool arg1)
{
    if (arg1) {
        ui->toolBox_apparence_global->show();
        ui->label_type->setText( tr("Graph") );
        ui->lineEdit_id->setText("");
    } else {
        ui->toolBox_apparence_global->hide();
    }
    ui->lineEdit_id->setDisabled(arg1);
}
