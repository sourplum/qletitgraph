#include "about.h"
#include "ui_about.h"
#if QT_VERSION >= 0x050000
#include <QWidget>
#else
#include <QtGui/QWidget>
#endif

About::About(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::About)
{
    ui->setupUi(this);
}

About::~About()
{
    delete ui;
}
