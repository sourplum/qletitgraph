#include "mainwindow.h"
#include <QApplication>
#include <QTranslator>
#include <QLocale>
#include <QLibraryInfo>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTranslator qtTranslator;
    qtTranslator.load("qletitgraph_" + QLocale::system().name());
    a.installTranslator(&qtTranslator);
    MainWindow w;
    w.show();

    return a.exec();
}
