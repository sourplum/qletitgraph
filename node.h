#ifndef NODE_H
#define NODE_H

// #include <QtGui>
#include <QColor>
#include <QVector3D>
#include <QRect>
#include <QPoint>

class Node
{

private:


public:
    QRect* hitBox;
    QVector3D topLeft;
    QVector3D topRight;
    QVector3D bottomRight;
    QVector3D bottomLeft;
    QColor color;
    Node();
    Node( QVector3D topLeft, QVector3D topRight, QVector3D bottomRight, QVector3D bottomLeft, QColor color );
    Node( QVector3D topLeft, float width, float height, QColor color );
    ~Node();
};
/*
 * glVertex3f(-1.0f, 1.0f, 0.0f);              // Top Left
 * glVertex3f( 1.0f, 1.0f, 0.0f);              // Top Right
 * glVertex3f( 1.0f,-1.0f, 0.0f);              // Bottom Right
 * glVertex3f(-1.0f,-1.0f, 0.0f);              // Bottom Left
 */
#endif // NODE_H

