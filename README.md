# QLetitGraph #

Qt Application using Opengl to render a 3d element controlled by the user with mouse and sliders.

### What is this repository for? ###

* Learn Qt
* Learn Opengl

### What was done ###

* Set up a Qt App
* Design the Interface
* Learn and Use slots() and signals()
* Use a QGLWidget
* Learn basic Opengl transformations
* Open a file using filters
